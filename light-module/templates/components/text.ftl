<div class="cms-component text-component">
    <div class="prose sm:prose lg:prose-xl max-w-none">
        <div style="width:${content.slider!}%">
            ${cmsfn.decode(content).text!}
        </div>
    </div>
</div>