<!DOCTYPE html>
<html lang="en">
<head>
    [@cms.init][/@cms.init]
    <meta charset="UTF-8">
    <title>${content.title!}</title>
</head>
<body>
<div id="main-content" class="area content-area">
    [@cms.area name="content-area"/]
</div>
</body>
</html>